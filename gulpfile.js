var path = require('path'),
	gulp = require('gulp'),
	concat = require('gulp-concat'),
	rename = require('gulp-rename'),
	filesize = require('gulp-filesize'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	cssnano = require('gulp-cssnano'),
	minifyCSS = require('gulp-minify-css'),
	minifyHTML = require('gulp-htmlmin'),
	uglify = require("gulp-uglify"),
	babel = require('gulp-babel'),
	browserSync = require('browser-sync'),
	watch = require('gulp-watch'),
	changed = require('gulp-changed'),
	imagemin = require('gulp-imagemin'),
	jpegtran = require('imagemin-jpegtran'),
	pngquant = require('imagemin-pngquant'),
	optipng = require('imagemin-optipng'),
	directoryProject = 'landingpagevarnish/_frontend/dist/';

gulp.task('html', function(){
	// move html to different platforms
	var opts = {removeComments: true, collapseWhitespace: true};

	gulp.src(['_frontend/src/**/*.html', '_frontend/src/**/*.json'])
	.pipe(changed('_frontend/dist', {extension: '.html'}))
	.pipe(minifyHTML(opts))
	.pipe(gulp.dest('_frontend/dist'))
	.on('error', function(err){
		console.log(err.message);
	});
});

gulp.task('fonts', function(){
	gulp.src([
		'components/components-font-awesome/fonts/**',
		'components/materialize/fonts/**',
		'_frontend/src/fonts/**'
	])
	.pipe(changed('_frontend/dist/assets/fonts'))
	.pipe(gulp.dest('_frontend/dist/assets/fonts'))
	// .pipe(gulp.dest('laravel/public/assets/fonts'))
	.on('error', function(err){
		console.log(err.message);
	});
});

gulp.task('pagespeed', function(){
	/*COMPRESSAO DE IMAGENS UPLOAD*/
	gulp.src(['laravel/public/assets/images/**'])
	.pipe(
		imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [
				optipng({quality: '60-70'}),
				pngquant({quality: '60-70', speed: 4}),
				jpegtran({quality: '60-70'})
			]
		})
	)
	.pipe(gulp.dest('min/images'))
	.on('error', function(err){
		console.log(err.message);
	});

	gulp.src(['laravel/public/upload/**'])
	.pipe(
		imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [
				optipng({quality: '60-70'}),
				pngquant({quality: '60-70', speed: 4}),
				jpegtran({quality: '60-70'})
			]
		})
	)
	.pipe(gulp.dest('min/upload'))
	.on('error', function(err){
		console.log(err.message);
	});
});

gulp.task('media', function(){
	gulp.src([
		'_frontend/src/images/**'
	])
	.pipe(changed('_frontend/dist/assets/images'))
	.pipe(gulp.dest('_frontend/dist/assets/images'))
	// .pipe(gulp.dest('laravel/public/assets/images'))
	.on('error', function(err){
		console.log(err.message);
	});

	gulp.src([
		'_frontend/src/videos/**'
	])
	.pipe(changed('_frontend/dist/assets/videos'))
	.pipe(gulp.dest('_frontend/dist/assets/videos'))
	// .pipe(gulp.dest('laravel/public/assets/videos'))
	.on('error', function(err){
		console.log(err.message);
	});
});

gulp.task('css', function(){
	gulp.src([
		'components/components-font-awesome/scss/font-awesome.scss',
		'components/materialize/sass/materialize.scss',
		'components/owl.carousel/dist/assets/owl.carousel.min.css',
		'components/owl.carousel/dist/assets/owl.theme.default.min.css',
		'_frontend/src/css/**',
		'_frontend/src/scss/style.scss'
	])
	.pipe(changed('_frontend/dist/assets/css', {extension: '.css'}))
	.pipe(sass())
	.pipe(autoprefixer('last 3 version'))
	.pipe(cssnano())
	.pipe(minifyCSS())
	.pipe(concat('style.min.css'))
	.pipe(filesize())
	.pipe(gulp.dest('_frontend/dist/assets/css'))
	// .pipe(gulp.dest('laravel/public/assets/css'))
	.on('error', function(err){
		console.log(err.message);
	});
});

gulp.task('library', function(){
	gulp.src([
		'components/jquery/dist/jquery.min.js',
		'components/Materialize/dist/js/materialize.js',
		'components/jquery-form/jquery.form.js',
		'components/jquery-mask-plugin/dist/jquery.mask.min.js',
		'components/jquery-validation/dist/jquery.validate.min.js',
		'components/printelement/dist/jquery.printelement.min.js',
		'components/owl.carousel/dist/owl.carousel.min.js',
		'_frontend/src/js/vendors/**/*.js'
	])
	.pipe(changed('_frontend/dist/assets/js', {extension: '.js'}))
	.pipe(concat('libraries.min.js'))
	.pipe(uglify())
	.pipe(filesize())
	.pipe(gulp.dest('_frontend/dist/assets/js'))
	// .pipe(gulp.dest('laravel/public/assets/js'))
	.on('error', function(err){
		console.log(err.message);
	});
});

gulp.task('js', function(){
	gulp.src([
		'_frontend/src/js/helpers/**/*.js',
		'_frontend/src/js/script.js'
	])
	.pipe(changed('_frontend/dist/assets/js', {extension: '.js'}))
	.pipe(babel({presets: ['babili']}))
	.pipe(concat('script.min.js'))
	.pipe(filesize())
	.pipe(gulp.dest('_frontend/dist/assets/js'))
	// .pipe(gulp.dest('laravel/public/assets/js'))
	.on('error', function(err){
		console.log(err.message);
	});
});

gulp.task('server', function(){
	//atualiza o navegador a cada alteracao
	browserSync.init(['_frontend/dist/**'], {
		port: 8080,
		proxy: 'http://localhost/' + directoryProject
	});
});

gulp.task('watch', function(){
	var html = gulp.watch(['_frontend/src/**/*.html', '_frontend/src/**/*.json'], ['html']);
	var media = gulp.watch(['_frontend/src/images/**', '_frontend/src/videos/**'], ['media']);
	var css = gulp.watch(['_frontend/src/scss/**', '_frontend/src/css/**'], ['css']);
	var js = gulp.watch(['_frontend/src/js/**', '_frontend/src/js/helpers/**/*.js'], ['js']);

	html.on('change', function(file){
		console.log('Event type: ' + file.type);
		console.log('Event path: ' + file.path);
	});
	media.on('change', function(file){
		console.log('Event type: ' + file.type);
		console.log('Event path: ' + file.path);
	});
	css.on('change', function(file){
		console.log('Event type: ' + file.type);
		console.log('Event path: ' + file.path);
	});
	js.on('change', function(file){
		console.log('Event type: ' + file.type);
		console.log('Event path: ' + file.path);
	});
});

// DEFAULT TASK
gulp.task('default', ['server', 'fonts', 'html', 'media', 'css', 'library', 'js', 'watch']);