/**
 *
 * ==============================================
 * SCROLLPIN V1.0.0
 *
 * @author TIAGO RODRIGO CALÓGERO
 * Senior Full Stack Developer and Software Engineer
 * http://curriculo.ogaiti.com.br
 * ==============================================
 *
 * require https://code.jquery.com/jquery-2.2.4.min.js
 * usage <div class="pin" data-offset-top="160" data-offset-bottom="20" data-margin="120">Element Pinned</div>
 * data-offset-bottom / fade, nothing, number
 *
 */

'use strict';

const scrollpin = $('.pin');

if(scrollpin.length){
	let pinned = (scrollTop, direction) =>{
		let newScrollTop = scrollTop;
		let directionScroll = direction;

		scrollpin.each((key, el) =>{
			//bugfix firefox
			const _leftEl = $(el).offset().left;

			let _pinned = ($(el).hasClass('scrollpinned'));
			let _pinnedTop = ($(el).hasClass('scrollpinned-top'));
			let _pinnedBottom = ($(el).hasClass('scrollpinned-bottom'));
			let _offsetTop = ($(el).data('offset-top'))
				? $(el).data('offset-top')
				: 0;

			let _offsetBottom = _offsetTop;
			if($(el).data('offset-bottom')){
				if($(el).data('offset-bottom') == 'fade')
					_offsetBottom = 0;
				else if($(el).data('offset-bottom') == 'nothing')
					_offsetBottom = 1;
				else
					_offsetBottom = $(el).data('offset-bottom');
			}

			let _margin = ($(el).data('margin'))
				? $(el).data('margin')
				: 0;
			let _nextOffset = ($(el).closest('section').next().length)
				? $(el).closest('section').next().offset().top
				: $('footer').offset().top;
			let _startPin = $(el).closest('section').offset().top + _offsetTop;
			let _endPin = _nextOffset - $(el).height() - _offsetBottom;

			//fixed top
			if((newScrollTop >= _startPin && directionScroll == 'down') || (newScrollTop <= _endPin && newScrollTop >= _startPin && directionScroll == 'up')){
				//0 = fade
				if(_pinned && _offsetBottom === 0)
					$(el).stop().fadeIn(600);
				else
					$(el).addClass('scrollpinned scrollpinned-top').removeClass('scrollpinned-bottom')
					.css({'position': 'fixed', 'top': _margin, 'bottom': 'inherit', 'left': _leftEl});
			}

			//stay down
			if(newScrollTop >= _endPin && directionScroll == 'down'){
				//0 = fade | 1 = nothing fixed
				if(_offsetBottom === 0)
					$(el).stop().fadeOut(400);
				else if(_offsetBottom === 1)
					return;
				else
					$(el).addClass('scrollpinned').removeClass('scrollpinned-bottom scrollpinned-top')
					.css({'position': 'absolute', 'bottom': _margin, 'top': 'inherit'});
			}

			//not fixed
			if(((newScrollTop <= _startPin && _pinnedTop) || (newScrollTop <= (_startPin - $(el).height()) && _pinnedBottom)) && _pinned){
				$(el).removeClass('scrollpinned scrollpinned-bottom scrollpinned-top')
				.removeAttr('style');
			}
		});
	};

	let directionScroll = 0;
	let direction = '';
	$(window).on('scroll resize load', () =>{
		let resolution = $(window).width();

		//funcionar somente se maior que ipad
		if(resolution > 798){
			let scroll = $(this).scrollTop();

			direction = (scroll > directionScroll) ? 'down' : 'up';
			directionScroll = scroll;
			pinned(scroll, direction);
		}
	});
}